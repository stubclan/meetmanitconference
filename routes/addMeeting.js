/*
 * GET home page.
 */
 var massive = require('massive');
 const connectionParms = {
   host: '10.146.85.68',
   port: 5432,
   database: 'smtd9',
   user: 'dev9',
   password: 'smtp9'
 };
 var db;
 massive(connectionParms).then(connectedDB => {
   console.log('Connected to db');
   db = connectedDB;
 }).catch(error => {
   console.log('Error connecting to db');
   console.log(error);
 });

exports.view = function(req, res){

	res.render('meetings');

};

exports.add = function(req, res) {


        var venueid = req.body.venueid;
        var meetingname = req.body.meetingname;
        var meetingdatestart = req.body.meetingdatestart;
        var meetingdateend = req.body.meetingdatestart;
        var meetinghours = req.body.meetinghours;
        var meetingsponsor = req.body.meetingsponsor;
        var meetingbudget = req.body.meetingbudget;

        roomQuery = 'INSERT INTO meeting("meetingid", "venueid", "meetingname", "meetingdatestart", "meetingdateend",\
        "meetinghours", "meetingsponsor", "meetingbudget") VALUES ((select MAX(meetingid) +1 from meeting) , $1, $2, $3, $4, $5, $6, $7)';

        myRender = 'meetings';

        db.query(roomQuery, [venueid, meetingname, meetingdatestart, meetingdateend,
        meetinghours, meetingsponsor, meetingbudget]).then(function(results) {
        		res.render(myRender, {rooms:results})
          }).catch(error => {
              console.log('Error updating db',error);
              res.status(500);
        });

};
