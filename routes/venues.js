
/*
 * GET home page.
 */
var massive = require('massive');
const connectionParms = {
	  host: '10.146.85.68',
	  port: 5432,
	  database: 'smtd9',
	  user: 'dev9',
	  password: 'smtp9'
};
var db;
massive(connectionParms).then(connectedDB => {
	  console.log('Connected to db');
	  db = connectedDB;
}).catch(error => {
	  console.log('Error connecting to db');
	  console.log(error);
});

exports.venues = function(req, res){

	var myID = req.query.id;

	if(myID) {
		roomQuery = 'select * \
						from eventroom left join venue on eventroom.venueid = venue.venueid left join meeting on meeting.meetingid = eventroom.meetingid \
						where meeting.meetingid = $1 \
						limit 1';

		myRender = 'venues/venuesMeeting';

		db.query(roomQuery, [myID]).then(function(results) {
			res.render(myRender,
				{venues: results })
		  }).catch(error => {
				console.log('Error querying db',error);
				res.status(500);
		 });
	}
	else {
		roomQuery = 'select * \
					from venue';

		myRender = 'venues/venues';

		db.query(roomQuery).then(function(results) {
			res.render(myRender,
				{venues: results })
		  }).catch(error => {
				console.log('Error querying db',error);
				res.status(500);
		 });
	}
};

exports.addVenuesForm = function(req, res){

		myRender = 'venues/addVenues';
		res.render(myRender);


};

exports.addVenues = function(req, res){

		var myID = req.query.id;
		var name = req.body.venueName;
		var address1 = req.body.venueAddress1;
		var address2 = req.body.venueAddress2;
		var city = req.body.venueCity;
		var state = req.body.venueState;
		var postal = req.body.venuePostal;
		var country = req.body.venueCountry;
		var phone = req.body.venuePhone;
		var fax = req.query.venueFax;
		var cateringCost = req.body.venueCateringCost;
		var sleepingRooms = req.body.venueSleepingRooms;



		roomQuery = 'INSERT INTO public.venue("venueid", "venuetypeid", "venuename", "venueaddress1", "venueaddress2", "venuecity", "venuestate", "venuepostal", "venuecountry", "venuephone", "venuefax", "venuecateringcost", "venuesleepingrooms") \
		VALUES ((select MAX(venueid) +1 from venue) , 1, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)';


		db.query(roomQuery, [name, address1, address2, city, state, postal, country, phone, fax, cateringCost, sleepingRooms]).then(function(results) {
				res.redirect('/venues');
		  }).catch(error => {
				console.log('Error querying db',error);
				res.status(500);
		 });
};
