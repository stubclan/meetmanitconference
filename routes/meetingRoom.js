
/*
 * GET home page.
 */
var massive = require('massive');
const connectionParms = {
	  host: '10.146.85.68',
	  port: 5432,
	  database: 'smtd9',
	  user: 'dev9',
	  password: 'smtp9'
};
var db;
massive(connectionParms).then(connectedDB => {
	  console.log('Connected to db');
	  db = connectedDB;
}).catch(error => {
	  console.log('Error connecting to db');
	  console.log(error);
});

exports.rooms = function(req, res){

	var myID = req.query.id;

	roomQuery = 'select * \
					from eventroom \
						left join venue on eventroom.venueid = venue.venueid \
						left join meeting on meeting.meetingid = eventroom.meetingid \
						where eventroom.meetingid = $1';

	myRender = 'meetingRoom';


    db.query(roomQuery, [myID]).then(function(results) {
			res.render(myRender,
				{rooms: results })
      }).catch(error => {
			console.log('Error querying db',error);
			res.status(500);
     });
};


exports.addRoom = function(req, res){

	if (res.method == 'POST') {
		var myID = req.query.id;
		roomQuery = 'Insert Query'; /////Insert query here

		myRender = 'meetingRoom';


	    db.query(roomQuery, [myID]).then(function(results) {
				res.render(myRender,
					{rooms: results })
	      }).catch(error => {
				console.log('Error querying db',error);
				res.status(500);
	     });
	}
	else {
		res.render('meeting/addMeeting');
	}


};


exports.editRoom = function(req, res){

	// var myID = req.query.id;
	//
	// roomQuery = 'Edit Query'; /////Insert query here
	//
	// myRender = 'meetingRoom';
	//
	//
    // db.query(roomQuery, [myID]).then(function(results) {
	// 		res.render(myRender,
	// 			{rooms: results })
    //   }).catch(error => {
	// 		console.log('Error querying db',error);
	// 		res.status(500);
    //  });

};

exports.deleteRoom = function(req, res){

	// var myID = req.query.id;
	//
	// roomQuery = 'Delete Query'; /////Insert query here
	//
	// myRender = 'meetingRoom';
	//
	//
    // db.query(roomQuery, [myID]).then(function(results) {
	// 		res.render(myRender,
	// 			{rooms: results })
    //   }).catch(error => {
	// 		console.log('Error querying db',error);
	// 		res.status(500);
    //  });
};
