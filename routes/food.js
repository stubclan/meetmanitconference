
/*
 * GET home page.
 */

 var massive = require('massive');
 const connectionParms = {
 	  host: '10.146.85.68',
 	  port: 5432,
 	  database: 'smtd9',
 	  user: 'dev9',
 	  password: 'smtp9'
 };
 var db;
 massive(connectionParms).then(connectedDB => {
 	  console.log('Connected to db');
 	  db = connectedDB;
 }).catch(error => {
 	  console.log('Error connecting to db');
 	  console.log(error);
 });



exports.foods = function(req, res){
	 var meetingid = req.query.id;
	roomQuery = 'select * from food where "venueID"=$1 order by "date"';

	myRender = 'food/foods';
    db.query(roomQuery,[meetingid]).then(function(results) {
			res.render(myRender,
				{foods: results, id: meetingid})
      }).catch(error => {
			console.log('Error querying db',error);
			res.status(500);
     });
	//res.render('food/foods');
};


exports.addFood = function(req, res){

	if (res.method == 'POST') {

		var myID = req.query.venueid;
		var foodName = req.query.foodName;
		var DayOfWeek = req.query.DayOfWeek;
		var date = req.query.date;

		roomQuery = 'INSERT INTO food( \
	"foodID", "venueID", "foodName", "DayOfWeek", "date") VALUES \
	(nextval("foodsequence"), $1, $2, $3, null); '; /////Insert query here

		myRender = 'food/addFood';

	    db.query(roomQuery, [myID,foodName,DayOfWeek,null]).then(function(results) {
				res.render(myRender,
					{rooms: results })
	      }).catch(error => {
				console.log('Error querying db',error);
				res.status(500);
	     });
	}
	else {
		res.render('food/addFood');
	}


};


exports.editFood = function(req, res){

	if (res.method == 'POST') {

		var myID = req.query.id;
		var foodName = req.query.foodName;
		var DayOfWeek = req.query.DayOfWeek;
		var date = req.query.date;

		roomQuery = 'select * from food where "foodID" = $1'; /////Insert query here

		myRender = 'food/editFood';

	    db.query(roomQuery, [myID]).then(function(results) {
				res.render(myRender,
					{foods: results })
	      }).catch(error => {
				console.log('Error querying db',error);
				res.status(500);
	     });
	}

	else {
		var myID = req.body.id;

		roomQuery = 'select * from food where "venueID"=$1'; /////Insert query here

		myRender = 'food/editFood';

	    db.query(roomQuery, [myID]).then(function(results) {
				res.render(myRender, {foods: results})
	      }).catch(error => {
				console.log('Error querying db',error);
				res.status(500);
	     });
	}

	// var myID = req.query.id;
	//
	// roomQuery = 'Edit Query'; /////Insert query here
	//
	// myRender = 'meetingRoom';
	//
	//
    // db.query(roomQuery, [myID]).then(function(results) {
	// 		res.render(myRender,
	// 			{rooms: results })
    //   }).catch(error => {
	// 		console.log('Error querying db',error);
	// 		res.status(500);
    //  });

	res.render('food/editFood');

};

exports.deleteFood = function(req, res){

	// var myID = req.query.id;
	//
	// roomQuery = 'Delete Query'; /////Insert query here
	//
	// myRender = 'meetingRoom';
	//
	//
    // db.query(roomQuery, [myID]).then(function(results) {
	// 		res.render(myRender,
	// 			{rooms: results })
    //   }).catch(error => {
	// 		console.log('Error querying db',error);
	// 		res.status(500);
    //  });

	res.render('food/deleteFood');
};
