var express = require('express');
var massive = require('massive');

var helloworld = require('./routes/helloworld');
var participants = require('./routes/participants');
var meetingRoom = require('./routes/meetingRoom');
var food = require('./routes/food');
var venues = require('./routes/venues');
var avEquipment = require('./routes/avEquipment');
var meetings = require('./routes/meetings');
var meetingDetails = require('./routes/meetingDetails');
var participantDetails = require('./routes/participantDetails');
var addMeeting = require('./routes/addMeeting');

var http = require('http');
var path = require('path');
var app = express();

// all environments settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.json());
app.use(express.urlencoded());
app.locals.moment = require('moment');
app.locals.accounting = require('accounting');
app.locals.numeral = require('numeral');

// This statement allows Session Variables to be used in Jade
app.use(function(req,res,next){
    res.locals.session = req.session;
    next();
});

app.use(express.static(path.join(__dirname, 'public')));

// Get and Post Route Statements

app.get('/helloworld', helloworld.hello);		  								//  Hello World Example
app.get('/participants', participants.list);      								//  Participants Listing
app.post('/participantDetails', participantDetails.postteam);                	//  Update Participant's Team


app.get('/meetingRoom', meetingRoom.rooms);                                     //  Meeting room display
app.post('/meetingRoom/add', meetingRoom.addRoom);                              //  Meeting room Add
app.post('/meetingRoom/edit', meetingRoom.editRoom);                               //  Meeting room Edit
app.post('/meetingRoom/delete', meetingRoom.deleteRoom);
app.get('/meetingRoom/add', meetingRoom.addRoom);                              //  Meeting room Add
app.get('/meetingRoom/edit', meetingRoom.editRoom);                               //  Meeting room Edit
app.get('/meetingRoom/delete', meetingRoom.deleteRoom);                        //  Meeting room Delete

app.get('/food', food.foods);                                     //  food display
app.post('/food/add', food.addFood);                              //  Food Add
app.post('/food/edit', food.editFood);                               //  Food Edit
app.post('/food/delete', food.deleteFood);
app.get('/food/add', food.addFood);                              //  Food Add
app.get('/food/edit', food.editFood);                               //  Food Edit
app.get('/food/delete', food.deleteFood);

app.get('/meetings', meetings.rooms);
app.get('/meetingDetails', meetingDetails.details);
app.get('/avEquipment', avEquipment.equipment);

app.get('/venues', venues.venues);
app.get('/venues/addForm', venues.addVenuesForm);
app.post('/venues/add', venues.addVenues);

app.post('/addMeeting/add', addMeeting.add);
app.get('/addMeeting', addMeeting.view);



// Default Route
app.get('/*', meetings.rooms);


// Start the Node Server on Port 3000
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
